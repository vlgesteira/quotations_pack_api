defmodule QuotationsPackApiWeb.PageController do
  use QuotationsPackApiWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
