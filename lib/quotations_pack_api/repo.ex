defmodule QuotationsPackApi.Repo do
  use Ecto.Repo,
    otp_app: :quotations_pack_api,
    adapter: Ecto.Adapters.MyXQL
end
