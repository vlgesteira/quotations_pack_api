use Mix.Config

# Configure your database
config :quotations_pack_api, QuotationsPackApi.Repo,
  username: System.get_env("MYSQL_USER"),
  password: System.get_env("MYSQL_PASS"),
  database: "quotations_pack_api_test",
  hostname: System.get_env("MYSQL_HOST"),
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :quotations_pack_api, QuotationsPackApiWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
